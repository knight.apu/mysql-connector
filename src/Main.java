import java.sql.*;
import java.util.*;


public class Main {



    public static void main(String[] args) {
        //sqlObject data = new sqlObject();
        List<sqlObject> sqldata = new ArrayList<>();

        try {
            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/languagedb", "root", "root");
            Statement myStmt = myConn.createStatement();
            String sql1 = "select content.text, content.language, content_contents.study_module_id" +
                    " from content Inner join content_contents on content.id = content_contents.contents_id";

            ResultSet myRs = myStmt.executeQuery(sql1);

//            String sql2 = "insert into apuTest" +
//                    "text, language, study_module_id" +
//                    "values ('"+ myRs.getString("text")+ "', '" + myRs.getString("language")
//                    + "', '" + myRs.getString("study_module_id")+ "')";

//            myStmt.executeUpdate("insert into apuTest (text, language_name, study_module_id)" +
//                    "values ('"+ myRs.getString("text")+ "', '" + myRs.getString("language")
//                    + "', '" + myRs.getString("study_module_id")+ "')");

            while (myRs.next()) {
                String text = myRs.getString("text");
                String language = myRs.getString("language");
                int studyModudeId = Integer.parseInt(myRs.getString("study_module_id")) ;
                sqlObject data = new sqlObject(text, language, studyModudeId);
                sqldata.add(data);
                //System.out.println(myRs.getString("text") + " || " + myRs.getString("language") + " || " + myRs.getString("study_module_id"));

            }
            for(int i=0; i<sqldata.size(); i++) {

                myStmt.executeUpdate("insert into apuTest (text, language_name, study_module_id)" +
                        "values ('"+ sqldata.get(i).getText()+ "', '" + sqldata.get(i).getLanguage()
                        + "', '" +sqldata.get(i).getStudyModuleId()+ "')");

                System.out.println(sqldata.get(i).getText() + "==" + sqldata.get(i).getLanguage() + "==" + sqldata.get(i).getStudyModuleId());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
