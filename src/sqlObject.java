public class sqlObject {


    String text;
    String language;
    int studyModuleId;

    public sqlObject(String text, String language, int studyModuleId) {
        this.text = text;
        this.language = language;
        this.studyModuleId = studyModuleId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getStudyModuleId() {
        return studyModuleId;
    }

    public void setStudyModuleId(int studyModuleId) {
        this.studyModuleId = studyModuleId;
    }


}